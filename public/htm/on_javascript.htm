<!DOCTYPE html>

<!--- openBSD uptime check --->

<html>

<head>

<meta name="viewport" content="width=device-width, initial-scale=1.0">

<link rel="stylesheet" href="/css/normalize.css">

<link rel="stylesheet" href="/css/stylize.css">

<title>THINK SILICON - on_javascript</title>

</head>

<body>


<header>
<a href="/"><h1>THINK SILICON</h1></a>
<tagline>a blog about explorations in software</tagline>
</header>
<hr>

<h2>THOUGHTS ON JAVASCRIPT</h2>

<p>In the past, my approach to Javascript was generally along the lines
of: avoid &amp; block, where possible. This was for the following reasons:</p>

<ul>
<li><p>Security: Javascript has made the open web executable, and has
brought about a complicated (and therefore bug ridden) Javascript
interpreter becoming a standard feature of any modern web browser</p></li>
<li><p>Privacy: browsers open up enormous amounts features to the
Javascript engines, allowing things such as <a rel="noopener noreferrer" href="https://panopticlick.eff.org">browser
fingerprinting</a></p></li>
<li><p>Performance: HTML &amp; CSS are extremely lightweight to parse and run
on modern hardware, due to the rendering being handled by skilled
programmers who architect browsers; Javascript on the open web,
however, can be written by amateurs and can cause websites to behave
sluggishly or to crash the browser</p></li>
</ul>

<p>However, as I have grown older I have realised that, as usual, the
situation of reality differs significantly from the theoretical
disadvantages, and the following things should be acknowledged:</p>

<ul>
<li><p>Servers and operating systems are high maintenance and not 100%
reliable, and as a result system administrators who tend to them in
a commercial context are prone to becoming overworked and bitter</p></li>
<li><p>There is a Javascript interpreter available absolutely everywhere
nowadays</p></li>
<li><p>Server-side rendering, in an internet context, is hard to run at a
consistently high capacity, as user demand for it fluctuates, yet
24/7 availability is expected, and so it is prone to waste, in terms
of hardware and electricity</p></li>
</ul>

<p>So, whereas in the past I believed that server-side processing was
correct, at the end of the day, the data inputs have to be processed
<em>somewhere</em>, and I have come around to client-side processing.</p>

<p>Lately, I have become interested in JAMstack - essentially a no-ops
way to run a website. A generic server handles over a bundle of
Javascript, and the client device takes it from there, contacting
third-party services hosted by <em>someone else</em>.</p>

<p>To actually get to something practical, I first checked out
"Javascript: The Good Parts", a book I had heard of long ago, infamous
for the constructive criticism it brought to the language, over a
decade ago. But I quickly realised the author has created a newer
book, "How Javascript Works", which covers more modern developments,
and found it to be a good intro. This covers the language from a
big-picture perspective, such as explaining how Javascript is less
stuck in the imperative paradigm than other mainstream languages, the
modern movement of the language towards functional programming
(e.g. variables can store functions), and more specific details such
as how numbers are handled (all floats). There are also explanations
about how Javascript is single-threaded - the language has no
multicore (for now) - but that it runs an event loop.</p>

<p>I then checked out the tutorial for <code>vue.js</code> - apparently the easiest
web framework to find your way around - and built a single-page
application that calculates <a rel="noopener noreferrer" href="https://postcode-calc.pages.dev">distances between postcodes</a>, by using Mapbox's API to
convert them to co-ordinates, making asynchronous HTTP requests at
scale with <code>fetch()</code>.</p>

<p>Something I found to be a muddle is the fact that asynchronous
execution is a first-class citizen in the world of Javascript - the
language is highly oriented around optimising network requests, and
less optimised around crunching numbers. The implications of this are
that calls to core functions such as <code>fetch()</code> will return only a
<code>Promise</code> which will only later be resolved, and instantly move on to
the next execution line. Essentially, what has to be turned on
manually with Python 3's <code>asyncio</code> is turned on by default with
Javascript. To add further confusion, Javascript does use the words
<code>async</code> and <code>await</code> with the same kind of style as Python 3, however
they work quite significantly differently - with Javascript, once you
initiate asynchronous execution, you cannot really collate the results
and then return to imperative execution, like the Python 3.</p>

<p>Javascript was designed to be asynchronous from the ground
up. However, with Python 3, asynchronous execution was an
afterthought, and is not (yet) backwards compatible with the majority
of the standard library, so even basic things like an asynchronous
HTTP request require either relying on third-party libraries or
manually implementing the HTTP protocol. I have not investigated how
Ruby 3 fares in this area.</p>

<p>I then picked up "Learning React", another O'Reilly book, which delves
into Meta's <a rel="noopener noreferrer" href="https://react.dev">React</a> library <code>react.js</code>, which is
appropriate for JAMstack applications. Eventually it became clear that
<code>react.js</code> alone is unmanageable, and it must be used with a "bundler"
to get <code>import</code> statements that work similarly to other scripting
languages. I tried various bundlers and frameworks and found them all
problematic - poorly documented or excessively complicated - until I
eventually discovered <a rel="noopener noreferrer" href="https://rollupjs.org">Rollup</a> which is simple
but effective, once you have a <a rel="noopener noreferrer" href="https://www.codeguage.com/blog/setup-rollup-for-react">working baseline</a>.
Then once you have modular code you can finally write unit tests,
using another good quality library from Meta named Jest. Why Meta did
not fund engineers to build a good bundler &amp; server, however, is
beyond me.</p>

<p>I briefly investigated Typescript, which is essentially a mild tweak
on Javascript to make it statically typed, but the whole book
"Learning Typescript" can pretty much be reduced to the fact that you
only have to specify types when writing functions, as types can be
derived implicitly elsewhere. Because of the fragmentation of the
ecosystem around Meta's React, for larger projects it seems like
Google's Angular (implemented in Typescript) would be a worthwhile
investigation, as it seems to be a more full implementation, but
apparently with a larger learning curve.</p>

<p>Below is an curious example of the kind of thing you can do with
Javascript, involving storing function calls inside a key-value data
structure. Basically, in one array of key-value data structures we
store data, then in a separate key-value data structure - with
equivalent key names - we store calls to functions to validate this
data. First we iterate over the array of validation functions, then
subiterate over the array of key-value elements, gradually calling the
associated validation functions for every key-value pair of every
element, and returning totals of valid data. This was done to create a
JSON editor with live data validation feedback (in summary format).</p>

<pre>

const data_arr = [
{
    "title": "Telephone",
    "advisor": "janice",
    "day_of_week": "monday",
    "time_start": 1000,
    "time_end": 1330,
    "postcode": "",
    "address": [],
    "latitude": 0,
    "longitude": 0
},
{
    "title": "Medical Centre",
    "advisor": "josh",
    "day_of_week": "tuesday",
    "time_start": 900,
    "time_end": 1700,
    "postcode": "SE1 1AA",
    "address": ["A Street"],
    "latitude": 51.40000,
    "longitude": -0.03000
},
{
    "title": "Library Centre",
    "advisor": "jillary",
    "day_of_week": "wednesday",
    "time_start": 1000,
    "time_end": 1600,
    "postcode": "NW1 2BB",
    "address": ["10 Long Road"],
    "latitude": 51.440000,
    "longitude": -0.020000
}
]

</pre>

<pre>

function validate_implicit (data_arr) {

    ////  Build an object to know which functions to use to validate
    ////  different categories of data.

    const validation_targets_implicit = {

    "day_of_week": (data) => {

        return validate_day(data) },

    "time_start":  (data) => {

        return validate_num_within_range(data, 0, 2400) },

    "time_end":    (data) => {

        return validate_num_within_range(data, 0, 2400) },

    "postcode":    (data) => {

        return validate_postcode(data) },

    "address":     (data) => {

        return validate_address(data) },

    "latitude":    (data) => {

        return validate_num_within_range(data, -1, 52) },

    "longitude":   (data) => {

        return validate_num_within_range(data, -1, 52) }

    }

    ////  Iterate over the categories of data.

    ////  Subiterate over the data array, checking if the data within
    ////  each element fits the validation for that category of data.

    ////  Returns an array, the size being the quantity of different
    ////  data categories.

    const validation_res_implicit = Object.keys(

    validation_targets_implicit).map(

    function(field_name) {

        return data_arr.reduce(

        function (total, element) {

            return (
            validation_targets_implicit[field_name]
                (
                element[field_name]
                ) === 1 ?
                total + 1 : total
            )

        }, 0

        )

    }

    )

    return validation_res_implicit;

    )

}

</pre>

<time>17 MAY 2023</time> <br> 

<nav>
<a href="/htm/posts-indexed.htm">back to index of posts</a>
</nav>


<hr class="right">

<footer>This responsive blog was created using <code>markdown</code>, <code>python3.11</code>, and <code>css</code> wizardry.</footer>


</body>
</html>
