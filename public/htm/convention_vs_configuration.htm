<!DOCTYPE html>

<!--- openBSD uptime check --->

<html>

<head>

<meta name="viewport" content="width=device-width, initial-scale=1.0">

<link rel="stylesheet" href="/css/normalize.css">

<link rel="stylesheet" href="/css/stylize.css">

<title>THINK SILICON - convention_vs_configuration</title>

</head>

<body>


<header>
<a href="/"><h1>THINK SILICON</h1></a>
<tagline>a blog about explorations in software</tagline>
</header>
<hr>



<h2>CONVENTION VS. CONFIGURATION</h2>

<p>Starting with a little background... How did I come to Linux? Well I sold a macbook and built an x86 desktop computer (AMD) for the first time, and Windows XP gradually became incredibly slow compared to a fresh install (perhaps as a result of running two anti-viruses). Then the OSX 10.6.8 unofficial AMD port I was using was stable but patchless. The next hacked together unofficial AMD ported version of OSX available was more unreliable, but equally untrustworthy at the kernel core. Then I first tried Linux (Ubuntu), in 2013. I recall trying out different desktop environments - <code>lxde</code>, <code>xfce</code>, <code>gnome</code> - by downloading them through Ubuntu's app store. Eventually Ubuntu crashed irrecoverably as a result of chopping and changing. I next tried out Fedora, with <code>openbox</code> window manager with a carefully customised <code>conker</code> interface, loading <code>c++</code> version of Firefox from a ramdisk. Unfortunately that one crashed on its own accord after an update, due to an error with <code>grub</code>, but not permanently. Then onto Arch Linux, in 2016, where I setup a system where all the partitions were different filesystems, and ran <code>grsecurity</code> mandatory access controls, back when it was open-source. The hard drive in that was a relatively young second-hand Hitachi, but died quite quickly too, compared to an identical model from the same seller. Too bad.</p>

<p>As I grew older, it became increasingly obvious that all these customisations were giving minimal gains at the cost of basic functionality. I began to realise life is short, and, as a result, came to the conclusion that increasing customisations decreases reliability. Nowdays I have more developed mathematical/statistical skills, and realise there is an entirely logical explanation for this.</p>

<p>Say we build a program with:</p>

<ul>
<li>3 boolean options (on/off command line flags, or checkboxes)</li>
<li>each one of these options enables something completely different</li>
<li>the user is allowed to enable any combination of them</li>
</ul>

<p>We instantly have the following quantity of possibilities:</p>

<p><code>3c0 + 3c1 + 3c2 + 3c3 = 8</code></p>

<p>...possible combinations</p>

<p>When you are initially learning statistics, you write these small-scale things out, to prove them to yourself.</p>

<pre>

o o o
x o o
o x o
o o x
x x o
o x x
x o x
x x x

</pre>

<p>OK good start. We could probably write tests for all these possible combinations, run them, and make sure they all work as expected.</p>

<p>Say instead of 3 options we have 30 options...</p>

<p><code>30c0 + 30c1 + 30c2 ... + 30c29 + 30c30 = 1,073,741,824</code></p>

<p>...possible combinations</p>

<p>Ah... Now we're instantly in a completely different realm of possible combinations. Sure, some combinations may be more popular than others, but instantly everything is so much harder to test.</p>

<p>Now lets push things, say we have 300 options...</p>

<pre>

#!/usr/bin/env python3.9

from math import comb

total = 0

for n in range(301):
    total += comb(300,n)

print(total)

</pre>

<p>Whilst we're on this topic, let's check <code>ruby</code> too (though the algorithm may be the same under the hood, and really, if this was something mission-critical, we should be directly implementing the mathematical algorithm in a language as close to hardware as possible, e.g. <code>C</code>)...</p>

<pre>

#!/usr/bin/env ruby30

def fact(x)

  if x == 0
    return 1

  else
    x * fact(x-1)

  end
end


total = 0

for r in 0..300

  total +=                       \
    fact(300) /                  \
    ( fact(r) * fact(300 - r) )

end

print(total)

</pre>

<p>Based on the statistics formula:</p>

<pre>

   n!
--------
r!(n-r)!

</pre>

<p><code>300c0 + 300c1 + 300c2 ... + 300c299 + 300c300 = 2037035976334486086268445688409378161051468393665936250636140449354381299763336706183397376</code></p>

<p>...possible combinations</p>

<p>Accuracy of the above number assumes computers work as expected!</p>

<p><code>*eyes closed, eyebrows raised, backbone upright*</code><br>
"Everything is completely under control!"<br>
<code>*computer explodes in background, like old-school Simpsons*</code></p>

<p>Now if we think about the number of possible different implementations (e.g. brands, models, versions) of motherboards, processors, firmware/microcode, compilers, kernels, libraries, and userland applications that can be combined together in different ways, we are beginning to hit a quantity of combinations that is much, much higher than 300, and that is theoretically impossibly high to manage. The Linux kernel alone has potentially thousands of choices at compile time. Theoretically, the quantity of combinations increasingly diverges towards infinity.</p>

<p>"There is no test that can prove that a program is free of defects. [...] we put programs into production that we know are not perfect, hoping that we will find and correct the errors before anyone finds out. That is crazy. That is the state of the art" - Douglas Crockford</p>

<p>Given the quantity of possibilities that are now in existence, in modern computing, we begin to wonder how any computer in the world works at all. Maybe the phrase "if you put enough monkeys in a room with a typewriter, one of them will eventually type up Shakespeare" is truly a fitting analogy for the modern state of computing. It is hard to compare the current state of things to a beehive, or an ant hill, because ants didn't develop things equivalent to skyscrapers and globally connected banks, in what could be described evolutionarily as "overnight".</p>

<p>"It doesn't work in theory, but it works in practice" - Doug Belshaw</p>

<p>But wait, my computers do work alot more reliably nowadays, I have barely any of the kinds of problems I used to. What changed? Well, me, I changed. I gave up on customisations where possible. I opted for (or diverged towards) simpler software with gradual-but-constant rates of change, with large enough user bases to test it, and more popular choices of hardware. I realised it is far easier to choose your set of defaults carefully (unless you have very pressing reasons to diverge from them), and change your own mindset, than to stray down the paths of abundant and unique combinations, spending hours to both set them up and then debug them...  "No one ever got fired for buying IBM" is about reducing the possible combinations, as is Apple's more self-disciplined attitude to releasing products compared to competitors, as is the <code>rails</code> tagline "convention over configuration", as is the UNIX philosophy "do one thing and do it well".</p>

<p>Of course, there is a security loss to uniformity, as, if everyone is running the same thing, everyone has the same bugs. But, given the quantity of possibilities, how could you even verify a piece of software or hardware doesn't have bugs to begin with? In the past, I have read of Linux kernel developer Greg Kroah-Haartman stating, on the topic of security bugs on a public mailing list (some years ago, link hard to find), that the choice is between "known bugs, and unknown bugs, and I feel more comfortable with the latter". Linus Torvalds has written about a feeling of thrill/anxiety when releasing kernels, in the past.</p>

<p>Maybe the reality is kind of like the monkeys came up with an amazing series of hacks so that they manage to type up the front and back pages consistently. The bulk of the middle pages are actually just a load of gibberish, formatted well enough at a glance, but most people are not <em>that</em> interested in Shakespeare enough to have noticed, and the researchers reward bananas out of amazement, and this creates an iterative feedback loop. Maybe they do actually end up typing up Shakespeare as a result.</p>

<p>Ironically, this blog is not barebones HTML, and was stylized some years ago, but I tried turning the CSS off and it became unreadable. If I were to write it from scratch, there'd be 50% less, and I am proactively cutting it back.</p>

<time>22 DEC 2022</time> <br> 

<nav>
<a href="/htm/posts-indexed.htm">back to index of posts</a>
</nav>


<hr class="right">

<footer>This responsive blog was created using <code>markdown</code>, <code>python3.11</code>, and <code>css</code> wizardry.</footer>


</body>
</html>
